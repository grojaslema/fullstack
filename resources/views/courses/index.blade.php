<div class="container">
    <h4>Lista de Cursos <a href="{{route('courses.create')}}">Registrar nuevo curso</a></h4>
    <hr>

    <div class="table-responsive">
        @if($data)
            <table class="table">
                <thead>
                <tr>
                    <td>id</td>
                    <td>nombre</td>
                    <td>Creado</td>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $$row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->created_at}}</td>
                        </tr>
                </tbody>
                @endforeach
            </table>
        @endif
    </div>
</div>