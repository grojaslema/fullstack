<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $faker = Faker::create();

        for ($i=0; $i <30; $i++){

            DB::table('course')->insert(array(
                'name' => $faker->randomDigitNotNull,
            ));
        }

    }
}
