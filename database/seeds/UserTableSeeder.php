<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $faker = Faker::create();

        for ($i=0; $i <30; $i++){

            DB::table('users')->insert(array(
                'name' => $faker->name,
                'last_name' => $faker->lastName,
                'email' => $faker->unique()->email,
                'password' => \Hash::make($faker->password),
                'permission' => $faker->boolean($chanceOfGettingTrue = 50)
            ));
        }

    }
}
