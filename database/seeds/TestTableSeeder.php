<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $faker = Faker::create();

        for ($i=0; $i <30; $i++){

            DB::table('test')->insert(array(
                'name' => $faker->safeColorName,
            ));
        }

    }
}
